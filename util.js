/**
 * 防抖函数
 * @param {执行函数} func
 * @param {函数参数} param
 * @param {等待时间} wait
 * @returns
 */
function debounce(func, param, wait) {
  func = func || ((_) => console.log("防抖函数"));
  param = param || null;
  wait = wait || 2000;
  clearTimeout(this.debounce[String(func)]);
  this.debounce[String(func)] = setTimeout(() => {
    func(param);
  }, wait);
}

/**
 * 节流函数
 * @param {执行函数} func
 * @param {函数参数} param
 * @param {间隔时间} wait
 */
function throttle(func, param, wait) {
  func = func || ((_) => console.log("节流函数"));
  param = param || null;
  wait = wait || 2000;
  if (
    this.throttle[String(func)] == undefined ||
    this.throttle[String(func)] == true
  ) {
    func(param);
    this.throttle[String(func)] = false;
    setTimeout(() => {
      this.throttle[String(func)] = true;
    }, wait);
  }
}

/**
 * 判断是否是空
 * @param {对象} obj
 * @returns
 */
function isEmpty(obj) {
  return typeof obj === "undefined" || obj === null || obj === "";
}

/**
 * 是否是对象函数
 * @param {对象} obj
 */
function isObj(obj) {
  return ["[object Object]"].includes(Object.prototype.toString.call(obj));
}

/**
 * 更新属性函数（深度搜索算法）
 * @param {目标属性} targetParams
 * @param {更新属性} params
 */
export function updateProps(targetParams, params) {
    if (!isObj(params)) return;
    Object.keys(params).forEach((key) => {
      if (isObj(params[key])) {
        targetParams[key] = targetParams?.[key] ?? {};
        updateProps(targetParams[key], params[key]);
      } else {
        if (targetParams[key] !== params[key]) {
          targetParams[key] = params[key];
        }
      }
    });
}

/**
 * 生成不重复唯一id
 * @returns 
 */
function onlyId(){
  return Date.now().toString(36) + Math.random().toString(36).slice(3);
}
